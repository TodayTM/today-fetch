# Today Fetch

## Dependencies:
- Gnu Coreutils
- BASH
- ripgrep

## Install
**WARNING!:** THIS SCRIPTS ASSUMES THAT YOU ARE USING .XINITRC (STARTX) TO START XORG.
```
cd ~
git clone https://gitlab.com/TodayTM/today-fetch.git
sh ~/today-fetch/install.sh
```

