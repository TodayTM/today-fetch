#!/usr/bin/env bash

# Store information
os=$(hostnamectl | awk '/Operating System:/ {print $3,$4,$5,$6,$7,$8,$9;}')
kernel=$(cat /proc/sys/kernel/osrelease)
shell=$(basename $SHELL)
wm=$(rg exec ~/.xinitrc | awk '{print $2}')
cpu=$(rg 'model name.*' /proc/cpuinfo | sed -n 1p | sed 's/.*:.//g')
ram=$(free -h | sed -n 2p | awk '{print $2}')
browser=$(basename $BROWSER)
terminal=$(basename $TERM)
editor=$(basename $EDITOR) 
user=$USER@$HOSTNAME
packages=$(pacman -Qq | wc -l)
	
# Colors
RED='\033[0;31m'
WHITE='\033[0;37m'
BLUE='\033[0;34m'

# Show Info
echo -e "${BLUE}            '#'                 ${WHITE}OS:${RED} $os"
echo -e "${BLUE}           '###'                ${WHITE}KERNEL:${RED} $kernel"
echo -e "${BLUE}          '#####'               ${WHITE}SHELL:${RED} $shell"
echo -e "${BLUE}         '#######'              ${WHITE}WM:${RED} $wm"
echo -e "${BLUE}        '#########'             ${WHITE}CPU:${RED} $cpu"
echo -e "${BLUE}       '###########'            ${WHITE}RAM:${RED} $ram"
echo -e "${BLUE}      '#############'           ${WHITE}BROWSER:${RED} $browser"
echo -e "${BLUE}     '#######  ######'          ${WHITE}TERMINAL:${RED} $terminal"
echo -e "${BLUE}    '#######    ######'         ${WHITE}EDITOR:${RED} $editor" 
echo -e "${BLUE}   '########    #######'        ${WHITE}PACKAGES:${RED} $packages"
echo -e "${BLUE}  '#####           #####'"
echo -e "${BLUE} '###                 ###'      $user"
