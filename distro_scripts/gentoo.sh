#!/usr/bin/env bash

# Store information
os=$(hostnamectl | awk '/Operating System:/ {print $3,$4,$5,$6,$7,$8,$9;}')
kernel=$(cat /proc/sys/kernel/osrelease)
shell=$(basename $SHELL)
wm=$(rg exec ~/.xinitrc | awk '{print $2}')
cpu=$(rg 'model name.*' /proc/cpuinfo | sed -n 1p | sed 's/.*:.//g')
ram=$(free -h | sed -n 2p | awk '{print $2}')
terminal=$(basename $TERM)
browser=$(basename $BROWSER)
editor=$(basename $EDITOR) 
user=$USER@$HOSTNAME
packages=$(dir ${br_prefix}/var/db/pkg/*/*/ | wc -l)
	
# Colors
RED='\033[0;31m'
WHITE='\033[0;37m'
PURPLE='\033[0;35m'

# Show Info
echo -e "${PURPLE}   '###########'                 ${WHITE}OS:${RED} $os"
echo -e "${PURPLE}  '#          #####'             ${WHITE}KERNEL:${RED} $kernel"
echo -e "${PURPLE} '#       ##      #####'         ${WHITE}SHELL:${RED} $shell"
echo -e "${PURPLE}  '#     ####         ###'       ${WHITE}WM:${RED} $wm"
echo -e "${PURPLE}   '#     ##             #'      ${WHITE}CPU:${RED} $cpu"
echo -e "${PURPLE}     '#                  #'      ${WHITE}RAM:${RED} $ram"
echo -e "${PURPLE}    '#                   #'      ${WHITE}BROWSER:${RED} $browser"
echo -e "${PURPLE}  '##                ###'        ${WHITE}TERMINAL:${RED} $terminal"
echo -e "${PURPLE} '#              ####'           ${WHITE}EDITOR:${RED} $editor"
echo -e "${PURPLE} '#           ####'              ${WHITE}PACKAGES:${RED} $packages"
echo -e "${PURPLE}  '#       ####'"
echo -e "${PURPLE}   '########'                    $user"
