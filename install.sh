#!/usr/bin/env bash
clear

#Colors
RED='\033[0;31m'
WHITE='\033[0;37m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'

echo -e "${RED}"
Menu(){

#Logo Display
echo -e "▄▄▄█████▓ ▒█████  ▓█████▄  ▄▄▄     ▓██   ██▓     █████▒▓█████▄▄▄█████▓ ▄████▄   ██░ ██ "
echo -e " ▓  ██▒ ▓▒▒██▒  ██▒▒██▀ ██▌▒████▄    ▒██  ██▒   ▓██   ▒ ▓█   ▀▓  ██▒ ▓▒▒██▀ ▀█  ▓██░ ██▒"
echo -e " ▒ ▓██░ ▒░▒██░  ██▒░██   █▌▒██  ▀█▄   ▒██ ██░   ▒████ ░ ▒███  ▒ ▓██░ ▒░▒▓█    ▄ ▒██▀▀██░"
echo -e " ░ ▓██▓ ░ ▒██   ██░░▓█▄   ▌░██▄▄▄▄██  ░ ▐██▓░   ░▓█▒  ░ ▒▓█  ▄░ ▓██▓ ░ ▒▓▓▄ ▄██▒░▓█ ░██ "
echo -e "  ▒██▒ ░ ░ ████▓▒░░▒████▓  ▓█   ▓██▒ ░ ██▒▓░   ░▒█░    ░▒████▒ ▒██▒ ░ ▒ ▓███▀ ░░▓█▒░██▓ "
echo -e "  ▒ ░░   ░ ▒░▒░▒░  ▒▒▓  ▒  ▒▒   ▓▒█░  ██▒▒▒     ▒ ░    ░░ ▒░ ░ ▒ ░░   ░ ░▒ ▒  ░ ▒ ░░▒░▒ "
echo -e "    ░      ░ ▒ ▒░  ░ ▒  ▒   ▒   ▒▒ ░▓██ ░▒░     ░       ░ ░  ░   ░      ░  ▒    ▒ ░▒░ ░ "
echo -e "   ░      ░ ░ ░ ▒   ░ ░  ░   ░   ▒   ▒ ▒ ░░      ░ ░       ░    ░      ░         ░  ░░ ░"
echo -e "             ░ ░     ░          ░  ░░ ░                   ░  ░        ░ ░       ░  ░  ░ "
echo -e "                   ░                ░ ░                               ░                 "

#Menu Display
echo -e "${BLUE}"
echo -e " ======================================= "
echo -e " = Welcome to Today Fetch's Installer! = "
echo -e " ======================================= "
echo
echo -e "${WHITE}[ 1 ] ${BLUE} Arch"
echo -e "${WHITE}[ 2 ] ${PURPLE} Gentoo"
echo -e "${WHITE}[ 3 ] ${RED} Exit"
echo

#Option
	echo -ne "${WHITE}Choose your distro!"
	echo
	read option
	case $option in
	1) Arch;;
	2) Gentoo;;
	3) clear; exit ;;
	*) clear; echo -e "${RED} Error: Invalid option!"; Menu ;;
	esac
}

Arch(){
	sudo cp ~/today-fetch/distro_scripts/arch.sh /usr/bin/fetch
	clear
	fetch
	echo
	echo -e "${BLUE} INSTALLED!"
}

Gentoo(){
	sudo cp ~/today-fetch/distro_scripts/gentoo.sh /usr/bin/fetch
	clear
	fetch
	echo
	echo -e "${PURPLE} INSTALLED!"
}

Menu
